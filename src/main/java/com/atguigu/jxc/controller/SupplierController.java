package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    //http://localhost:8080/supplier/list
    @PostMapping("list")
    public Map<String,Object> list(Integer page, Integer rows, String supplierName, HttpServletRequest request){
        String page1 = request.getParameter("page");
        System.out.println("page1====" + page1);
        Map<String,Object> map = supplierService.list(page,rows,supplierName);
        return map;
    }
    //http://localhost:8080/supplier/save?supplierId=1
    @PostMapping("save")
    public ServiceVO saveOrUpdate(Supplier supplier,Integer supplierId){
        if (supplierId == null){
            //添加
            supplierService.save(supplier);
        }else {
            //修改
            supplierService.update(supplierId,supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }
    //http://localhost:8080/supplier/delete
    @PostMapping("delete")
    public ServiceVO delete(String ids){
        String[] split = ids.split(",");
        Boolean result = supplierService.delete(split);
        if (result){
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
        }else {
            return new ServiceVO(ErrorCode.CONSTRAINT_VIOLATION_CODE, ErrorCode.CONSTRAINT_VIOLATION_MESS,null);
        }
    }
}
