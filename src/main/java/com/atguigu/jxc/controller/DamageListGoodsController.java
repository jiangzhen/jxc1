package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;
    /**
     * 保存报损单
     * 请求参数：DamageList damageList, String damageListGoodsStr
     */
    //http://localhost:8080/damageListGoods/save?damageNumber=BS1605766644460（报损单号,前端生成）
    @PostMapping("save")
    public ServiceVO save(String damageListGoodsStr, DamageList damageList, HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());
        damageListGoodsService.saveDamage(damageList,damageListGoodsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);
    }
    /**
     * 报损单查询t_damage_list
     * 请求参数：String  sTime（开始时间）, String  eTime（结束时间）
     */
    //http://localhost:8080/damageListGoods/list
    @PostMapping("list")
    public Map<String,Object> list(String sTime,String eTime,HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        String trueName = user.getTrueName();
        Map<String,Object> map = damageListGoodsService.damageList(sTime,eTime,trueName);
        return map;
    }
    /**
     * 查询报损单商品信息
     * 请求参数：Integer damageListId（报损单Id）
     * getDamageGoodsByDamageListId
     */
    //http://localhost:8080/damageListGoods/goodsList

    @PostMapping("goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        Map<String,Object> map = damageListGoodsService.goodsList(damageListId);
        return map;
    }
}
