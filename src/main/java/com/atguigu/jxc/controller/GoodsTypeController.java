package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {
    @Autowired
    private GoodsTypeService goodsTypeService;
    /**
     * 查询所有分类,树形结构
     */
    //http://localhost:8080/goodsType/loadGoodsType
    @PostMapping("loadGoodsType")
    public List<GoodsTypeVo> loadGoodsType(){
        List<GoodsTypeVo> goodsTypeVoList = goodsTypeService.loadGoodsType();

        return goodsTypeVoList;
    }

    //http://localhost:8080/goodsType/save
    /**
     * 新增分类
     * 请求参数：String  goodsTypeName,Integer  pId
     */
    @PostMapping("save")
    public ServiceVO save(GoodsType goodsType){

        Boolean result = goodsTypeService.save(goodsType);
        if (result){
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
        }else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE,ErrorCode.REQ_ERROR_MESS,null);
        }
    }
    //http://localhost:8080/goodsType/delete
    /**
     * 删除分类
     * 请求参数：Integer  goodsTypeId
     */
    @PostMapping("delete")
    public ServiceVO delete(Integer goodsTypeId){

        Boolean result = goodsTypeService.delete(goodsTypeId);
        if (result){
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
        }else {
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS,null);
        }
    }
}
