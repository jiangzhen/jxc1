package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    //请求URL：http://localhost:8080/goods/listInventory
    @PostMapping("listInventory")
    //@ResponseBody
    public Map<String,Object> listInventory(Integer page,Integer rows,String codeOrName,Integer goodsTypeId){
        System.out.println("hhhhhjz-productg");
        Map<String,Object> map = goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
        return map;
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    //http://localhost:8080/goods/list
    @PostMapping("list")
    public Map<String,Object> list(Integer page,Integer rows,String goodsName,Integer goodsTypeId){
        Map<String,Object> map = goodsService.list(page,rows,goodsName,goodsTypeId);
        return map;
    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    //http://localhost:8080/goods/save?goodsId=37
    @PostMapping("save")
    public ServiceVO save(Goods goods,Integer goodsId){

        if (goodsId == null){
            //添加
            goodsService.save(goods);
        }else {
            //保存
            goodsService.update(goodsId,goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */
     //http://localhost:8080/goods/delete
    @PostMapping("delete")
    public ServiceVO delete(Integer goodsId){
        //有入库,进货,销售单据不能删除 state =1入库 =2表示有进货或销售单据
        if (goodsId == null){
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS,null);
        }
        Goods goods = goodsService.getById(goodsId);
        Integer state = goods.getState();
        if (state == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS,null);
        }
        if (state == 2){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS,null);
        }
        goodsService.delete(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //http://localhost:8080/goods/getNoInventoryQuantity
    @PostMapping("getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map = goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    //http://localhost:8080/goods/getHasInventoryQuantity
    @PostMapping("getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map = goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
        return map;
    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    //http://localhost:8080/goods/saveStock?goodsId=25
    @PostMapping("saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,Double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    //http://localhost:8080/goods/deleteStock
    @PostMapping("deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        //有入库,进货,销售单据不能删除 state =1入库 =2表示有进货或销售单据
        if (goodsId == null){
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS,null);
        }
        Goods goods = goodsService.getById(goodsId);
        Integer state = goods.getState();
        if (state == 1){
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS,null);
        }
        if (state == 2){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS,null);
        }
        goodsService.deleteStock(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }

    /**
     * 查询库存报警商品信息:当前库存量 小于 库存下限
     * 参数:无
     * @return
     */
    //http://localhost:8080/goods/listAlarm
    @PostMapping("listAlarm")
    public Map<String,Object> listAlarm(){
        Map<String,Object> map =goodsService.listAlarm();
        return map;
    }

}
