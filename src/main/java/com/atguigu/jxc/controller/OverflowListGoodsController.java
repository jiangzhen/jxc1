package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {
    @Autowired
    private OverflowListGoodsService overflowListGoodsService;
    /**
     * 新增报溢单
     * 请求参数：OverflowList overflowList, String overflowListGoodsStr
     */
    //http://localhost:8080/overflowListGoods/save?overflowNumber=BY1605767033015（报溢单号）
    @PostMapping("save")
    public ServiceVO save(String overflowListGoodsStr, OverflowList overflowList, HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());
        overflowListGoodsService.saveDamage(overflowList,overflowListGoodsList);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS,null);

    }
    /**
     *报溢单查询
     * 请求参数：String  sTime（开始时间）, String  eTime（结束时间）
     */
    //http://localhost:8080/overflowListGoods/list
    @PostMapping("list")
    public Map<String,Object> list(String sTime, String eTime, HttpSession session){
        User user = (User) session.getAttribute("currentUser");
        String trueName = user.getTrueName();
        Map<String,Object> map = overflowListGoodsService.overflowList(sTime,eTime,trueName);
        return map;
    }
    /**
     * 报溢单商品信息
     *请求参数：Integer overflowListId
     */
    //http://localhost:8080/overflowListGoods/goodsList
    @PostMapping("goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        Map<String,Object> map = overflowListGoodsService.goodsList(overflowListId);
        return map;
    }
}
