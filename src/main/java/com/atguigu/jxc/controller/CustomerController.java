package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    //http://localhost:8080 /customer/list
    @PostMapping("list")
    public Map<String,Object> list(Integer page,Integer rows,String customerName){
        Map<String,Object> map = customerService.list(page,rows,customerName);
        return map;
    }

    //http://localhost:8080/ customer/save?customerId=1
    @PostMapping("save")
    public ServiceVO saveOrUpdate(Customer customer,Integer customerId){

        if (customerId == null){
            //添加
            customerService.save(customer);
        }else {
            //修改
            customerService.update(customerId,customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
    }
    //http://localhost:8080/customer/delete
    @PostMapping("delete")
    public ServiceVO delete(String ids){
        String[] split = ids.split(",");
        Boolean result = customerService.delete(split);
        if (result){
            return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS,null);
        }else {
            return new ServiceVO(ErrorCode.CONSTRAINT_VIOLATION_CODE, ErrorCode.CONSTRAINT_VIOLATION_MESS,null);
        }
    }
}
