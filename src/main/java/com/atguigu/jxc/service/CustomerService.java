package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 带条件的分页查询
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    /**
     * 保存
     * @param customer
     */
    void save(Customer customer);

    /**
     * 修改
     * @param customerId
     * @param customer
     */
    void update(Integer customerId, Customer customer);

    Boolean delete(String[] split);
}
