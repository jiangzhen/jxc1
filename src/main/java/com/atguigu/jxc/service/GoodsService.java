package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();

    /**
     * 商品库存分页带条件查询
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 商品列表查询
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    void save(Goods goods);

    void update(Integer goodsId, Goods goods);

    Goods getById(Integer goodsId);

    void delete(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    void deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
