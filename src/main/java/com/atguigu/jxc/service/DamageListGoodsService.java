package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;
import java.util.Map;

public interface DamageListGoodsService {
    /**
     * 保存报损单,两张表
     * @param damageList
     * @param damageListGoodsList
     */
    void saveDamage(DamageList damageList, List<DamageListGoods> damageListGoodsList);

    Map<String, Object> damageList(String sTime, String eTime, String trueName);

    Map<String, Object> goodsList(Integer damageListId);
}
