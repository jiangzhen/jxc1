package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    /**
     * 带条件的分页查询
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    Map<String, Object> list(Integer page, Integer rows, String supplierName);

    /**
     * 保存供应商信息
     * @param supplier
     */
    void save(Supplier supplier);

    /**
     * 修改供应商信息
     * @param supplierId
     * @param supplier
     */
    void update(Integer supplierId, Supplier supplier);

    /**
     * 批量/单个删除
     * @param ids
     */
    Boolean delete(String[] ids);
}
