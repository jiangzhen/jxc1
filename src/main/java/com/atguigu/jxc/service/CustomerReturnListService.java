package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.CustomerReturnList;

import java.util.List;

public interface CustomerReturnListService {
    List<CustomerReturnList> selectList(List<Integer> idList);
}
