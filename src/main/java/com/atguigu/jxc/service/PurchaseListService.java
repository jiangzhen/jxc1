package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.PurchaseList;

import java.util.List;

public interface PurchaseListService {
    List<PurchaseList> selectList(List<Integer> supplierIdList);
}
