package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;

import java.util.List;

public interface GoodsTypeService {
    List<GoodsTypeVo> loadGoodsType();

    /**
     * 新增分类
     * @param goodsType
     */
    Boolean save(GoodsType goodsType);

    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    Boolean delete(Integer goodsTypeId);
}
