package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.service.CustomerReturnListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerReturnListServiceImpl implements CustomerReturnListService {
    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;
    @Override
    public List<CustomerReturnList> selectList(List<Integer> idList) {
        List<CustomerReturnList> customerReturnListList = customerReturnListGoodsDao.selectCustomerReturnListByCustomerIdList(idList);
        return customerReturnListList;
    }
}
