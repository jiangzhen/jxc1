package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.PurchaseListService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Autowired
    private PurchaseListService purchaseListService;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }

        Integer offset = (page -1) * rows;
        List<Supplier> supplierList = supplierDao.list(offset,rows,supplierName);
        Integer count = supplierDao.count(supplierName);
        map.put("total",count);
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public void save(Supplier supplier) {
        if (supplier !=null){
            supplierDao.save(supplier);
        }
    }

    @Override
    public void update(Integer supplierId, Supplier supplier) {
        if (supplierId != null && supplier != null){
            supplier.setSupplierId(supplierId);
            supplierDao.update(supplier);
        }
    }

    @Override
    public Boolean delete(String[] ids) {
        if (ids != null && ids.length>0){
            List<Integer> supplierIdList = Arrays.stream(ids).map(id -> Integer.parseInt(id)).collect(Collectors.toList());
            //判断是否有外键supplier_id
            List<PurchaseList> purchaseListList = purchaseListService.selectList(supplierIdList);
            if (!CollectionUtils.isEmpty(purchaseListList)){
                //存在外键
                return false;
            }else {
                //可以删除
                supplierDao.deleteByIdList(supplierIdList);
                return true;
            }
        }
        return false;
    }
}
