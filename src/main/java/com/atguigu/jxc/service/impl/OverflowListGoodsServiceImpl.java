package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDamage(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList) {
        overflowListDao.save(overflowList);
        overflowListGoodsList.forEach(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.save(overflowListGoods);
        });
    }

    @Override
    public Map<String, Object> overflowList(String sTime, String eTime, String trueName) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowListList = overflowListDao.selectListByDate(sTime,eTime);
        if (!CollectionUtils.isEmpty(overflowListList)){
            overflowListList.forEach(overflowList -> overflowList.setTrueName(trueName));
        }
        map.put("rows",overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.getOverflowGoodsByOverflowListId(overflowListId);
        map.put("rows",overflowListGoodsList);
        return map;
    }
}
