package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveDamage(DamageList damageList, List<DamageListGoods> damageListGoodsList) {
        //需要设置主键回填
        damageListDao.save(damageList);
        damageListGoodsList.forEach(damageListGoods -> {
            //
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.save(damageListGoods);
        });
    }

    @Override
    public Map<String, Object> damageList(String sTime, String eTime, String trueName) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageListList = damageListDao.selectListByDate(sTime,eTime);
        if (!CollectionUtils.isEmpty(damageListList)){
            damageListList.forEach(damageList -> damageList.setTrueName(trueName));
        }
        map.put("rows",damageListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageGoodsByDamageListId(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }
}
