package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<GoodsTypeVo> loadGoodsType() {
        //查询所有分类数据
        List<GoodsType> goodsTypeList = goodsTypeDao.findAll();
        //根据条件逐层children 赋值
        List<GoodsTypeVo> goodsTypeVoList = new ArrayList<>();
        //分组,key=pid
        Map<Integer, List<GoodsType>> collect = goodsTypeList.stream().collect(Collectors.groupingBy(GoodsType::getPId));
        if (!CollectionUtils.isEmpty(collect)){
            collect.get(-1).forEach(goodsType -> {
                GoodsTypeVo goodsTypeVo = new GoodsTypeVo();
                goodsTypeVoList.add(goodsTypeVo);
                //赋值
                goodsTypeVo.setId(goodsType.getGoodsTypeId());
                goodsTypeVo.setText(goodsType.getGoodsTypeName());
                Integer state = goodsType.getGoodsTypeState();
                goodsTypeVo.getAttributes().put("state",state);
                if (state == 0){
                    goodsTypeVo.setState("open");
                }else {
                    goodsTypeVo.setState("closed");
                    this.setChildren(collect,goodsTypeVo);
                }
            });
        }

        return goodsTypeVoList;
    }

    /**
     * 新增分类
     * 考虑改变父类的state:有子节点1 无子节点0
     * @param goodsType
     */
    @Override
    public Boolean save(GoodsType goodsType) {
        if (goodsType == null){
            return false;
        }
        //判断pid的是否运行添加
        //先查询父节点
        GoodsType goodsTypeParent = goodsTypeDao.getById(goodsType.getPId());
        if (goodsTypeParent.getPId() > 1){
            //不能超过三级节点
            return false;
        }
        //goodsType goodTypeName pid + state
        goodsType.setGoodsTypeState(0);
        //insert
        goodsTypeDao.save(goodsType);
        if (goodsTypeParent.getGoodsTypeState() !=1){
            goodsTypeParent.setGoodsTypeState(1);
            goodsTypeDao.updateGoodsTypeState(goodsTypeParent);
        }
        return true;
    }

    @Override
    public Boolean delete(Integer goodsTypeId) {
        //分析 先查询,判断有无子分类  判断此分类下有无数据
        GoodsType goodsType = goodsTypeDao.getById(goodsTypeId);
        if (goodsType.getGoodsTypeState() == 1){
            //有子分类
            return false;
        }
        //根据商品的类别查询商品的个数
        Integer count = goodsDao.getCountByGoodsTypeId(goodsTypeId);
        if (count == null || count == 0){
            //可以删除
            goodsTypeDao.delete(goodsTypeId);
            //删除后查询父节点 还有没有子节点 如果没有 修改其状态
            GoodsType goodsTypeParent = goodsTypeDao.getById(goodsType.getPId());
            List<Integer> goodsTypeIdList = goodsTypeDao.getSubTypeByGoodsTypeId(goodsTypeParent.getGoodsTypeId());
            if (CollectionUtils.isEmpty(goodsTypeIdList)){
                //修改其状态
                goodsTypeParent.setGoodsTypeState(0);
                goodsTypeDao.updateGoodsTypeState(goodsTypeParent);
            }
            return true;
        }else {
            return false;
        }
    }

    /**
     * 给children赋值-->递归调用
     * @param collect
     * @param goodsTypeVo
     */
    private void setChildren(Map<Integer, List<GoodsType>> collect, GoodsTypeVo goodsTypeVo) {
        List<GoodsTypeVo> subList = collect.get(goodsTypeVo.getId()).stream().map(goodsType -> {
            GoodsTypeVo subGoodsTypeVo = new GoodsTypeVo();
            //赋值
            subGoodsTypeVo.setId(goodsType.getGoodsTypeId());
            subGoodsTypeVo.setText(goodsType.getGoodsTypeName());
            Integer state = goodsType.getGoodsTypeState();
            subGoodsTypeVo.getAttributes().put("state",state);
            if (state == 0){
                subGoodsTypeVo.setState("open");
            }else {
                subGoodsTypeVo.setState("closed");
                this.setChildren(collect,subGoodsTypeVo);
            }
            return subGoodsTypeVo;
        }).collect(Collectors.toList());
        goodsTypeVo.setChildren(subList);
    }
}
