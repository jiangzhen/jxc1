package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.CustomerReturnList;
import com.atguigu.jxc.service.CustomerReturnListService;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private CustomerReturnListService customerReturnListService;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }
        Integer offSet = (page-1)*rows;
        List<Customer> customerList = customerDao.list(offSet,rows,customerName);
        Integer count = customerDao.getCount(customerName);
        map.put("total",count);
        map.put("rows",customerList);
        return map;
    }

    @Override
    public void save(Customer customer) {
        if (customer!= null){
            customerDao.save(customer);
        }
    }

    @Override
    public void update(Integer customerId, Customer customer) {
        if (customerId != null && customer != null){
            customer.setCustomerId(customerId);
            customerDao.update(customer);
        }
    }

    @Override
    public Boolean delete(String[] ids) {
        if (ids != null && ids.length>0){
            List<Integer> idList = Arrays.stream(ids).map(id -> Integer.parseInt(id)).collect(Collectors.toList());
            //判断是否有外键
            List<CustomerReturnList> customerReturnListList = customerReturnListService.selectList(idList);
            if (!CollectionUtils.isEmpty(customerReturnListList)){
                //存在外键
                return false;
            }else {
                //可以删除
                customerDao.deleteByIdList(idList);
                return true;
            }
        }
        return false;
    }
}
