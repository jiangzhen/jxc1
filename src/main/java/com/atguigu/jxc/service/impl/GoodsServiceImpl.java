package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CategoryDao;
import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.CustomerReturnListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private SaleListGoodsDao saleListGoodsDao;
    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;
    @Autowired
    private CategoryDao categoryDao;
    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }
        Integer offSet = (page-1)*rows;
        //rows = 10;
        //判断对应多个goodsTypeId ,可能选中前面的大类
        List<Integer> goodsTypeIdList = null;
        if (goodsTypeId != null ){
            if (goodsTypeId == 1){
                goodsTypeId = null;
            }else {
                goodsTypeIdList = categoryDao.selectListGoodsTypeId(goodsTypeId);
                if (!goodsTypeIdList.contains(goodsTypeId)){
                    goodsTypeIdList.add(goodsTypeId);
                }
            }
        }
        //分页查询list,查询总记录数count
        List<Goods> goodsList = goodsDao.listInventory(offSet,rows,codeOrName,goodsTypeIdList);
        Integer count = goodsDao.getCount(codeOrName,goodsTypeIdList);
        //查询售卖的数量
        //退货
        List<CustomerReturnListGoods> customerReturnListGoodsList = customerReturnListGoodsDao.selectRetrunCountList();
        //key goodsId,v num
        Map<Integer, Integer> returnMap = customerReturnListGoodsList.stream().collect(Collectors.toMap(CustomerReturnListGoods::getGoodsId, CustomerReturnListGoods::getGoodsNum));
        //卖出
        List<SaleListGoods> saleListGoodsList = saleListGoodsDao.selectSaleCount();
        Map<Integer, Integer> saleNumMap = saleListGoodsList.stream().collect(Collectors.toMap(SaleListGoods::getGoodsId, SaleListGoods::getGoodsNum));
        //卖出减去顾客退回
        returnMap.forEach((goodsId,num)->{
            Integer saleNum = saleNumMap.get(goodsId);
            saleNumMap.put(goodsId,saleNum - num);
        });
        //商品的卖出赋值
        for (Goods goods : goodsList) {
            boolean result = saleNumMap.containsKey(goods.getGoodsId());
            if (result){
                goods.setSaleTotal(saleNumMap.get(goods.getGoodsId()));
            }else {
                goods.setSaleTotal(0);
            }
        }
        //销售总数 t_sale_list_goods
        map.put("rows",goodsList);
        map.put("total",count);
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }
        Integer offSet = (page-1)*rows;
        //判断对应多个goodsTypeId ,可能选中前面的大类
        List<Integer> goodsTypeIdList = null;
        if (goodsTypeId != null ){
            if (goodsTypeId == 1){
                goodsTypeId = null;
            }else {
                goodsTypeIdList = categoryDao.selectListGoodsTypeId(goodsTypeId);
                if (!goodsTypeIdList.contains(goodsTypeId)){
                    goodsTypeIdList.add(goodsTypeId);
                }
            }
        }
        //分页查询list,查询总记录数count
        List<Goods> goodsList = goodsDao.listIdList(offSet,rows,goodsName,goodsTypeIdList);
        Integer count = goodsDao.getGoodsCountIdList(goodsName,goodsTypeIdList);
        //销售总数 t_sale_list_goods
        map.put("rows",goodsList);
        map.put("total",count);
        return map;
    }

    @Override
    public void save(Goods goods) {
        if (goods !=null){
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goodsDao.save(goods);
        }

    }

    @Override
    public void update(Integer goodsId, Goods goods) {
        if (goods != null){
            goods.setGoodsId(goodsId);
            //上一次采购价格
            Goods lastGoods = goodsDao.getById(goodsId);
            //每次修改都改变差价
            goods.setLastPurchasingPrice(lastGoods.getPurchasingPrice());
            //上一次的采购价格和本次不等,修改上一次的采购价格后再保存
            /*if (lastGoods.getPurchasingPrice() != goods.getPurchasingPrice()){
                goods.setLastPurchasingPrice(lastGoods.getPurchasingPrice());
            }else {
                goods.setLastPurchasingPrice(lastGoods.getLastPurchasingPrice());
            }*/
            goodsDao.update(goods);
        }
    }

    @Override
    public Goods getById(Integer goodsId) {
        return goodsDao.getById(goodsId);
    }

    @Override
    public void delete(Integer goodsId) {
        goodsDao.delete(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }
        Integer offSet = (page-1)*rows;
        List<Goods> goodsList = goodsDao.selectNoInventoryGoods(offSet,rows,nameOrCode);
        Integer count = goodsDao.selectNoInventoryCount(nameOrCode);
        map.put("rows",goodsList);
        map.put("total",count);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        if (page == null){
            page = 1;
        }
        if (rows == null){
            rows = 30;
        }
        Integer offSet = (page-1)*rows;
        List<Goods> goodsList = goodsDao.selectHasInventoryGoods(offSet,rows,nameOrCode);
        Integer count = goodsDao.selectHasInventoryCount(nameOrCode);
        map.put("rows",goodsList);
        map.put("total",count);
        return map;
    }

    /**
     * 添加库存是否要修改state = 2? 不明确 感觉不用呢
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        //修改成本价也要记录上一次的价格
        Goods goods = goodsDao.getById(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setLastPurchasingPrice(goods.getPurchasingPrice());
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.saveStock(goods);
    }

    /**
     * 删除库存
     * @param goodsId
     */
    @Override
    public void deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.listAlarm();
        map.put("rows",goodsList);
        return map;
    }


}
