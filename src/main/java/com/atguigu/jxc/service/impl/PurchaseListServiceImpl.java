package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.PurchaseListGoodsDao;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.service.PurchaseListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseListServiceImpl implements PurchaseListService {
    @Autowired
    private PurchaseListGoodsDao purchaseListGoodsDao;
    @Override
    public List<PurchaseList> selectList(List<Integer> supplierIdList) {
        return purchaseListGoodsDao.selectPurchaseListBySupplierIdList(supplierIdList);
    }
}
