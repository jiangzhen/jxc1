package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;
import java.util.Map;

public interface OverflowListGoodsService {
    /**
     * 保存报溢单 两张表
     * @param overflowList
     * @param overflowListGoodsList
     */
    void saveDamage(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList);

    Map<String, Object> overflowList(String sTime, String eTime, String trueName);

    Map<String, Object> goodsList(Integer overflowListId);
}
