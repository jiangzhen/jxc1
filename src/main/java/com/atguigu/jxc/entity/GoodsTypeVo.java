package com.atguigu.jxc.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class GoodsTypeVo {
    private Integer id;
    private String text;
    private String state;
    private String iconCls = "goods-type";
    private Map<String,Integer> attributes = new HashMap();
    private List<GoodsTypeVo> children;
}
