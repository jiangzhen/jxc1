package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    /**
     * 待条件的分页查询
     * @param offSet 偏移量
     * @param rows
     * @param codeOrName
     * @param goodsTypeIdList
     * @return
     */
    List<Goods> listInventory(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("codeOrName") String codeOrName,@Param("goodsTypeIdList") List<Integer> goodsTypeIdList);

    /**
     * 带条件查询库存商品总条数
     * @param codeOrName
     * @param goodsTypeIdList
     * @return
     */
    Integer getCount(@Param("codeOrName") String codeOrName,@Param("goodsTypeIdList") List<Integer> goodsTypeIdList);

    List<Goods> list(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsCount(@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> listIdList(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("goodsName") String goodsName,@Param("goodsTypeIdList") List<Integer> goodsTypeIdList);

    Integer getGoodsCountIdList(@Param("goodsName") String goodsName,@Param("goodsTypeIdList") List<Integer> goodsTypeIdList);

    Integer getCountByGoodsTypeId(@Param("goodsTypeId") Integer goodsTypeId);


    void save(Goods goods);

    void update(Goods goods);

    Goods getById(@Param("goodsId") Integer goodsId);

    void delete(@Param("goodsId") Integer goodsId);

    List<Goods> selectNoInventoryGoods(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer selectNoInventoryCount(@Param("nameOrCode") String nameOrCode);

    List<Goods> selectHasInventoryGoods(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer selectHasInventoryCount(@Param("nameOrCode") String nameOrCode);

    void saveStock(Goods goods);

    void deleteStock(@Param("goodsId") Integer goodsId);

    List<Goods> listAlarm();
}
