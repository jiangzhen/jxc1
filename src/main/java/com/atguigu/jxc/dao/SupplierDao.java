package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 供应商管理模块
 */
public interface SupplierDao {

    List<Supplier> list(@Param("offset") Integer offset,@Param("rows") Integer rows,@Param("supplierName") String supplierName);

    Integer count(@Param("supplierName") String supplierName);

    void save(Supplier supplier);

    void update(Supplier supplier);

    void delete(Integer supplierId);

    void deleteByIdList(@Param("supplierIdList") List<Integer> supplierIdList);
}
