package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {



    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    List<GoodsType> findAll();

    GoodsType getById(@Param("goodsTypeId") Integer goodsTypeId);

    void save(GoodsType goodsType);

    void delete(@Param("goodsTypeId") Integer goodsTypeId);

    List<Integer> getSubTypeByGoodsTypeId(@Param("goodsTypeId") Integer goodsTypeId);



}
