package com.atguigu.jxc.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryDao {
    List<Integer> selectListGoodsTypeId(@Param("goodsTypeId") Integer goodsTypeId);

}
