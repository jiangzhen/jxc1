package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    void save(DamageListGoods damageListGoods);

    List<DamageListGoods> getDamageGoodsByDamageListId(@Param("damageListId") Integer damageListId);
}
