package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {

    List<Customer> list(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("customerName") String customerName);

    Integer getCount(@Param("customerName") String customerName);

    void save(Customer customer);

    void update(Customer customer);

    void delete(@Param("customerId") int customerId);

    void deleteByIdList(@Param("idList") List<Integer> idList);
}
