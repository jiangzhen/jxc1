package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListDao {
    void save(DamageList damageList);

    List<DamageList> selectListByDate(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
