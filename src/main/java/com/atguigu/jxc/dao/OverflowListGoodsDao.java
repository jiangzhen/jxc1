package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    void save(OverflowListGoods overflowListGoods);

    List<OverflowListGoods> getOverflowGoodsByOverflowListId(@Param("overflowListId") Integer overflowListId);

}
